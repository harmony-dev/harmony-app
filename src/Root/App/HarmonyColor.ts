import { Color } from '@material-ui/core';

export const HarmonyDark: Color = {
	50: '#e2e2e3',
	100: '#b8b7b8',
	200: '#888789',
	300: '#585759',
	400: '#353336',
	500: '#110f12',
	600: '#0f0d10',
	700: '#0c0b0d',
	800: '#0a080a',
	900: '#050405',
	A100: '#ff4dff',
	A200: '#ff1aff',
	A400: '#e600e6',
	A700: '#cd00cd',
};
